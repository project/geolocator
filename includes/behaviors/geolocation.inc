<?php

/**
 * @file
 * Implementation of OpenLayers behavior.
 */

/**
 * Navigation Behavior
 */
class geolocator_behavior_geolocation extends openlayers_behavior {
  /**
   * Provide initial values for options.
   */
  function options_init() {
    return array(
    );
  }

  /**
   * Render.
   */
  function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'geolocator') .'/js/geolocation/Geolocation.js');
    drupal_add_js(drupal_get_path('module', 'geolocator') .'/js/geolocation/Control/Geolocation.js');
    drupal_add_js(drupal_get_path('module', 'geolocator') .'/js/geolocation/Geolocation/Base.js');
    drupal_add_js(drupal_get_path('module', 'geolocator') .'/js/geolocation/Geolocation/Gears.js');
    drupal_add_js(drupal_get_path('module', 'geolocator') .'/js/geolocation/Geolocation/W3C.js');
    drupal_add_js(drupal_get_path('module', 'geolocator') .'/js/geolocation/geolocator.js');
    return $this->options;
  }
}

