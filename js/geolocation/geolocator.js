
/**
 * @file
 * JS Implementation of OpenLayers behavior.
 */

/**
 * Navigation Behavior
 */
Drupal.behaviors.geolocator_behavior_geolocation = function(context) {
  var data = $(context).data('openlayers');
  if (data && data.map.behaviors['geolocator_behavior_geolocation']) {
    // Add control
    var control = new OpenLayers.Control.Geolocation();
    data.openlayers.addControl(control);
    control.activate();
  }
}
