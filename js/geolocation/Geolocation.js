OpenLayers.Geolocation = function() {
    var geolocations = ['W3C', 'Gears'];
    var geolocation = null;

    for (var i = 0, len = geolocations.length; i < len; i++) {
        var geolocClass = OpenLayers.Geolocation[geolocations[i]];
        if (geolocClass && geolocClass.prototype.supported()) {
            geolocation = new geolocClass();
            break;
        }
    }
    if (geolocation) {
        return geolocation;
    } else {
        throw "No geolocation backend available";
    }
};

OpenLayers.Geolocation.Response = OpenLayers.Class({
        
    /**
     * Property: success
     * {Boolean} 
     */
    success: null,

    /**
     * Property: lonlat
     * {<OpenLayers.LonLat>} 
     */
    lonlat: null,

    /**
     * Property: timestamp
     * {Date} 
     */
    timestamp: null,

    /**
     * Property: accuracy
     * {} 
     */
    accuracy: null,

    /**
     * Property: altitude
     * {} 
     */
    altitude: null,

    /**
     * Property: altitudeAccuracy
     * {} 
     */
    altitudeAccuracy: null,

    /**
     * Property: heading
     * {} 
     */
    heading: null,

    /**
     * Property: speed
     * {} 
     */
    speed: null,

    /**
     * Property: raw
     * {} 
     */
    raw: null,

    initialize: function(options) {
        OpenLayers.Util.extend(this, options);
    },

    CLASS_NAME: "OpenLayers.Geolocation.Response"
});
