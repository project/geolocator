/*
 * @requires OpenLayers/Control.js
 * @requires OpenLayers/Layer/Vector.js
 */

// draws the user position on the map using a vector layer

OpenLayers.Control.Geolocation = OpenLayers.Class(OpenLayers.Control, {

    geolocation: null,
    geolocations: ['W3C', 'Gears'],

    layer: null,

    initialize: function(options) {
        OpenLayers.Control.prototype.initialize.apply(this, [options]);

        for (var i = 0, len = this.geolocations.length; i < len; i++) {
            var geolocClass = OpenLayers.Geolocation[this.geolocations[i]];
            if (geolocClass && geolocClass.prototype.supported()) {
                this.geolocation = new geolocClass();
                break;
            }
        }
        // TODO: check this.geolocation
    },

    /**
     * Method: setMap
     * Set the map property for the control.
     *
     * Parameters:
     * map - {<OpenLayers.Map>}
     */
    setMap: function(map) {
        OpenLayers.Control.prototype.setMap.apply(this, arguments);

        var context = {
            getSize: function(feature) {
                if (feature.attributes.role == 'position') {
                    return 2;
                }
                
                if (feature.attributes.role == 'accuracy') {
                    var scale = feature.layer.map.getScale();

                    return feature.attributes.accuracy / scale;
                }
                return 1;
            },

            getColor: function(feature) {
                if (feature.attributes.role == 'position') {
                    return "red";
                }
                
                if (feature.attributes.role == 'accuracy') {
                    return "blue";
                }
                return '#ee9900';
            }
            
        };

        var template = {
            pointRadius: "${getSize}",
            fillColor: "${getColor}",
            fillOpacity: 0.4
        };

        var style = new OpenLayers.Style(template, {context: context});

        this.layer = new OpenLayers.Layer.Vector('Your Position', {
            styleMap: new OpenLayers.StyleMap(style)
        });


        this.map.addLayer(this.layer);
    },

    destroy: function() {
        this.deactivate();

        this.layer.destroy();
        this.layer = null;

        this.geolocation.destroy();
        this.geolocation = null;
        
        OpenLayers.Control.prototype.destroy.apply(this, arguments);
    },

    activate: function() {
        if (OpenLayers.Control.prototype.activate.apply(this, arguments)) {
            this.layer.setVisibility(true);
            this.watchId = this.geolocation.getCurrentPosition(OpenLayers.Function.bind(this.drawPosition, this), 
                                                               {loop: true})
            // TODO: start watching position
            return true;
        } else {
            return false;
        }
    },

    deactivate: function() {
        if (OpenLayers.Control.prototype.deactivate.apply(this, arguments)) {
            this.geolocation.stopWatching(this.watchId);
            this.layer.setVisibility(false);
            return true;
        } else {
            return false;
        }
    },

    drawPosition: function(response) {
        // todo: remove old features 
        if (response.success) {
            var point = new OpenLayers.Geometry.Point(response.lonlat.lon, 
                                                      response.lonlat.lat);
            console.log(response);
            var position = new OpenLayers.Feature.Vector(point, {role: 'position'});
            var accuracy = new OpenLayers.Feature.Vector(point, {
                role: 'accuracy', 
                accuracy: response.accuracy
            });
            this.layer.addFeatures([accuracy]);
        } else {
            alert("error code: " + response.raw.code);
        }
    },
    CLASS_NAME: "OpenLayers.Control.Geolocation"
});
