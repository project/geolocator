/**
 * @requires OpenLayers/Geolocation.js
 */

/**
 * Class: OpenLayers.Geolocation.Base
 * Base class for browser geolocation services.
 */
OpenLayers.Geolocation.Base = OpenLayers.Class({

    /**
     * Constant: EVENT_TYPES
     */
    EVENT_TYPES: ["positionchanged"],

    /**
     * APIProperty: events
     * {<OpenLayers.Events>}
     */
    events: null,

    /**
     * APIProperty: eventListeners
     */
    eventListeners: null,

    /**
     * Constructor: OpenLayers.Geolocation.Base
     *
     * Parameters:
     * options - {Object}
     */
    initialize: function(options) {
        OpenLayers.Util.extend(this, options);

        this.events = new OpenLayers.Events(this, this.div, // FIXME 
                                            this.EVENT_TYPES);
        if (this.eventListeners instanceof Object) {
            this.events.on(this.eventListeners);
        }
    },

    /**
     * APIMethod: destroy
     * Clean up.
     */
    destroy: function() {
        this.stopWatching();

        if (this.events) {
            if (this.eventListeners) {
                this.events.un(this.eventListeners);
            }
            this.events.destroy();
        }
        this.eventListeners = null;
        this.events = null;
    },

    /**
     * APIMethod: supported
     * This should be overridden by specific subclasses
     *
     * Returns:
     * {Boolean} The browser supports the geolocation backend.
     */
    supported: function() {
        return false;
    },

    getCurrentPosition: function(successCallback, errorCallback, options) {
    },

    stopWatching: function() {
    },

    CLASS_NAME: "OpenLayers.Geolocation.Base"
});
