/**
 * @requires OpenLayers/Geolocation.js
 */

/**
 * Class: OpenLayers.Geolocation.Gears
 */
OpenLayers.Geolocation.Gears = OpenLayers.Class(OpenLayers.Geolocation.Base, {


    /**
     * Property: geo
     * {Geolocation} 
     */
    geo: null,

    initialize: function(options) {
        if (this.supported()) {
            OpenLayers.Geolocation.Base.prototype.initialize.apply(this, [options]);
            this.geo = google.gears.factory.create('beta.geolocation');
        }
    },

    /**
     * APIMethod: supported
     *
     * Returns:
     * {Boolean} The browser supports the geolocation backend.
     */
    supported: function() {
        return !!(window.google && google.gears);
    },

    getCurrentPosition: function(callback, options) {
        var successCallback = OpenLayers.Function.bind(function(position) {
             this.onNewPosition.apply(this, [callback, position]);
        }, this);

        var errorCallback = OpenLayers.Function.bind(function(error) {
             this.onPositionError.apply(this, [callback, error]);
        }, this);

        if (options && options.loop) {
            return this.geo.watchPosition(successCallback,
                                          errorCallback,
                                          options);
        } else {
            this.geo.getCurrentPosition(successCallback,
                                        errorCallback,
                                        options);
            return null;
        }
    },

    stopWatching: function(watchId) {
        this.geo.clearWatch(watchId);
    },

    onNewPosition: function(callback, position) {
        var response = new OpenLayers.Geolocation.Response({
            success: true,
            raw: position,

            lonlat: new OpenLayers.LonLat(position.longitude,
                                          position.latitude),
            timestamp: position.timestamp,
            accuracy: position.accuracy,

            altitude: position.altitude,
            altitudeAccuracy: position.altitudeAccuracy
        });

        this.events.triggerEvent("positionchanged", {
            response: response
        });

        if (callback) {
            callback(response);
        }
    },

    onPositionError: function(callback, error) {
        var response = new OpenLayers.Geolocation.Response({
            success: false,
            timestamp: new Date(),
            raw: error
        });

        this.events.triggerEvent("positionchanged", {
            response: response
        });

        if (callback) {
            callback(response);
        }
    },

    CLASS_NAME: "OpenLayers.Geolocation.W3C"
});
