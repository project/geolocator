/**
 * @requires OpenLayers/Geolocation.js
 */

/**
 * Class: OpenLayers.Geolocation.W3C
 */
OpenLayers.Geolocation.W3C = OpenLayers.Class(OpenLayers.Geolocation.Base, {

    initialize: function() {
    },

    /**
     * APIMethod: supported
     *
     * Returns:
     * {Boolean} The browser supports the geolocation backend.
     */
    supported: function() {
        return !!(navigator.geolocation);
    },

    getCurrentPosition: function(callback, options) {        
        var successCallback = OpenLayers.Function.bind(function(position) {
             this.onNewPosition.apply(this, [callback, position]);
        }, this);

        var errorCallback = OpenLayers.Function.bind(function(error) {
             this.onPositionError.apply(this, [callback, error]);
        }, this);
        
        if (options && options.loop) {
            return navigator.geolocation.watchPosition(successCallback, 
                                                       errorCallback,
                                                       options);
        } else {
            navigator.geolocation.getCurrentPosition(successCallback, 
                                                     errorCallback,
                                                     options);
            return null;
        }
    },

    stopWatching: function(watchId) {
        navigator.geolocation.clearWatch(watchId);
    },

    onNewPosition: function(callback, position) {
        var response = new OpenLayers.Geolocation.Response({
            success: true,
            raw: position,

            lonlat: new OpenLayers.LonLat(position.coords.longitude,
                                          position.coords.latitude),
            timestamp: position.timestamp,
            accuracy: position.coords.accuracy,

            altitude: position.coords.altitude,
            altitudeAccuracy: position.coords.altitudeAccuracy,

            heading: position.coords.heading,
            speed: position.coords.speed
        });

        this.events.triggerEvent("positionchanged", {
            response: response
        });

        if (callback) {
            callback(response);
        }
    },

    onPositionError: function(callback, error) {
        var response = new OpenLayers.Geolocation.Response({
            success: false,
            timestamp: new Date(),
            raw: error
        });

        this.events.triggerEvent("positionchanged", {
            response: response
        });
        if (callback) {
            callback(response);
        }
    },

    CLASS_NAME: "OpenLayers.Geolocation.W3C"
});
